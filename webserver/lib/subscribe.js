/**
 * Created by Patrick on 2015-02-27.
 */
'use strict';

var axon = require('axon');
var socket = axon.socket('sub');

socket.connect('http://localhost:8081');

socket.on('error', function(err){
    throw err;
});

module.exports = socket;