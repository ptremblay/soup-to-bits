/**
 * Created by Patrick on 2015-02-25.
 */
'use strict';

var axon = require('axon');
var socket = axon.socket('pub');

socket.bind(8081);

exports.send = function(badge){
    socket.send(badge);
};