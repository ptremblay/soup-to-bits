/**
 * Created by Patrick on 2015-02-24.
 */
'use strict';

var redis = require('redis');
var client = redis.createClient(13501,'pub-redis-13501.us-east-1-2.4.ec2.garantiadata.com', {});
client.auth('redisdev',function(){
    console.log("Authenticated.");

});

client.on('error', function(err){
    throw err;
});

module.exports = client;
